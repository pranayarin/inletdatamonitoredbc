Please refer 'readme' file for more clean explanation.
This is a openFoam compatible boundary condition. Most of the time, the users have the experimental data at inlet and it needs to be implemented at the inlet very easily. This boundary condition caters to that. But it has been developed for inlet vector field like velocity etc. 

The user input dictionary should look like as follows:

    inlet                                             //patch name
    {
        type            expDataCentricVectorBC;       //boundary condition name (experimental data centric vector boundary condition)
        n               (1 0 0);                     //Flow direction
        rval            (0 0.5 1 1.5 2 2.5);         //radial location
        varVal          (50 40 35 30 20 10);         //corresponding variable value
        value           (0 0 0);                     // default velocity value
        bslOrcentr      centre;                      //data is from centre line (symmetry) or from a base line
                                                     // the input can be 'bsl' or 'centre'. bsl means from baseline.
                                                                                            centre means from centre of the patch.
    }

Descriptions:
point 1.//
'n' means the flow direction.
If it is along 'x' direction (1 0 0)
               'y' direction (0 1 0)
               'z' direction (0 0 1)
point 2.//

The baseline and centre may have any co-ordinate(x,y,z) while constructing your mesh. But this boundary condition will treat baseline and centre as (0,0,0) always. Hence No need to worry regarding coordinates and start providing the rval and varVal value considering the baseline and centre as (0,0,0) only. In the example provided, you can see list of rval and varVal having 6 entries. As we've chosen 'centre', hence at 0, velocity value will be 50m/s. At 0.5 metre of radial location from centre, the veclocity value will be 40m/s etc. Like these there are data upto 2.5m and the velocity upto 10 m/s.

if you choose 'bsl', the data will be from baseline '0' where vwlocity will be 50 m/s to the highest point of 2.5m where velocity will be 10 m/sall along it's length.

point 3.//
'value' entry;
It is the default value, the user wants provide. e.g (60 0 0) means ux = 60, uy = 0 , uz = 0; Most of the time apart from the experimental data, the usr has bulk velocity information at inlet. The user can provide this bulk data in this entry. If there is nothing, leave this as (0 0  0). 
Compile it in your user library.

Caution: The rval and varVal must have same size of data entries, otherwise it will fail.
